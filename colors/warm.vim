" Name: Warm
" Version: 0.1.0
" Description: A vim colorscheme
"
" Author: Agustin Jimenes <agustin_jimenez@tutanota.com>
" Licence: MIT
" Contributing: Contributors are too wellcome
"
"   * See more in https://gitlab.com/agustinjimenez/warm
"

" Global settings
" ------------------
    set background=dark
    let g:colors_name = "warm"

    if exists("syntax_on")
      syntax reset
    endif


" Colors definitions
" ------------------

    let s:dark_brown = { "cterm": 95, "gui": "#613838" }
    let s:purple = { "cterm": 141, "gui": "#8B61A8" }
    let s:light_green = { "cterm": 148, "gui": "#A4E400" }
    let s:light_blue = { "cterm": 81, "gui": "#62D8F1" }
    let s:magenta = { "cterm": 197, "gui": "#FC1A70" }
    let s:orange = { "cterm": 208, "gui": "#FF9700" }
    let s:yellow = { "cterm": 230, "gui": "#F2F298" }

    let s:default_grey = { "cterm": 248, "gui": "#B29F9F" }
    let s:light_grey = { "cterm": 7, "gui": "#bcbcbc" }
    let s:grey = { "cterm": 8, "gui": "#726F6F" }
    let s:dark_grey = { "cterm": 59, "gui": "#516457" }
    let s:darker_grey = { "cterm": 240, "gui": "#464646" }
    let s:light_charcoal = { "cterm": 236, "gui": "#292929" }
    let s:charcoal = { "cterm": 234, "gui": "#1F1F1F" }
    let s:black = { "cterm": 0, "gui": "#000000" }

    let s:danger = { "cterm": 203, "gui": "#F24B4B" }
    let s:white = { "cterm": 15, "gui": "#FFFFFF" }
    let s:red = { "cterm": 196, "gui": "#870000" }
    let s:dark_red = { "cterm": 88, "gui": "#870000" }
    let s:blood_red = { "cterm": 52, "gui": "#5f0000" }
    let s:green = { "cterm": 36, "gui": "#005f00" }
    let s:light_sea_blue = { "cterm": 32, "gui": "#0087ff" }
    let s:sea_blue = { "cterm": 25, "gui": "#005faf" }

    let s:none = { "cterm": "NONE", "gui": "NONE" }
    let s:bold = { "cterm": "bold", "gui": "bold" }
    let s:underline = { "cterm": "underline", "gui": "underline" }
    let s:bold_underline = { "cterm": "bold,underline", "gui": "bold,underline" }

    function! Highlight(group, fg, bg, style)
      exec "hi " . a:group
            \ . " ctermfg=" . a:fg["cterm"]
            \ . " ctermbg=" . a:bg["cterm"]
            \ . " cterm=" . a:style["cterm"]
            \ . " guifg=" . a:fg["gui"]
            \ . " guibg=" . a:bg["gui"]
            \ . " gui=" . a:style["gui"]
    endfunction



" Colors assignament
" ------------------

    " Global --------------------------------------------------
        call Highlight("Normal", s:default_grey, s:none, s:none)
        call Highlight("Special", s:danger, s:none, s:none)
        call Highlight("Title", s:default_grey, s:none, s:bold)
        call Highlight("Directory", s:danger, s:none, s:none)

        call Highlight("Whitespace", s:dark_grey, s:none, s:none)
        call Highlight("Cursor", s:charcoal, s:light_blue, s:none)

        call Highlight("Underlined", s:none, s:none, s:underline)
        call Highlight("Label", s:dark_brown, s:none, s:none)
        call Highlight("Todo", s:orange, s:none, s:bold)
        call Highlight("Tag", s:danger, s:none, s:none)

        call Highlight("Conceal", s:none, s:none, s:none)
        call Highlight("Type", s:none, s:none, s:none)

        call Highlight("SpellBad", s:default_grey, s:danger, s:none)
        call Highlight("SpellRare", s:default_grey, s:danger, s:none)
        call Highlight("SpellCap", s:charcoal, s:orange, s:none)
        call Highlight("SpellLocal", s:charcoal, s:orange, s:none)

    " Data Types --------------------------------------------------
        call Highlight("Constant", s:danger, s:none, s:none)
        call Highlight("Boolean", s:danger, s:none, s:none)
        call Highlight("Character", s:danger, s:none, s:none)

        call Highlight("Float", s:danger, s:none, s:none)
        call Highlight("Number", s:danger, s:none, s:none)
        call Highlight("String", s:dark_brown, s:none, s:bold)

    " Programming logic -------------------------------------------
        call Highlight("Keyword", s:danger, s:none, s:none)
        call Highlight("Include", s:danger, s:none, s:none)
        call Highlight("Define", s:danger, s:none, s:none)

        call Highlight("Exception", s:danger, s:none, s:bold)
        call Highlight("MatchParen", s:danger, s:none, s:bold_underline)
        call Highlight("Conditional", s:danger, s:none, s:none)
        call Highlight("Debug", s:danger, s:none, s:none)

        call Highlight("Delimiter", s:danger, s:none, s:none)
        call Highlight("Macro", s:danger, s:none, s:none)
        call Highlight("Operator", s:danger, s:none, s:none)
        call Highlight("PreProc", s:danger, s:none, s:none)

        call Highlight("Statement", s:danger, s:none, s:bold)
        call Highlight("Repeat", s:danger, s:none, s:none)
        call Highlight("Comment", s:dark_brown, s:none, s:none)
        call Highlight("Function", s:default_grey, s:none, s:underline)

        call Highlight("Identifier", s:grey, s:none, s:bold_underline)
        call Highlight("Question", s:grey, s:none, s:bold_underline)
        call Highlight("StorageClass", s:grey, s:none, s:bold_underline)
        call Highlight("Structure", s:grey, s:none, s:bold_underline)

    " Editor stuff -------------------------------------------
        call Highlight("Folded", s:dark_grey, s:none, s:none)

        call Highlight("Pmenu", s:default_grey, s:darker_grey, s:none)
        call Highlight("PmenuSel", s:charcoal, s:dark_grey, s:none)
        call Highlight("PmenuSbar", s:none, s:grey, s:none)
        call Highlight("PmenuThumb", s:none, s:default_grey, s:none)

        call Highlight("CursorLine", s:none, s:black, s:none)
        call Highlight("CursorLineNR", s:dark_brown, s:none, s:none)
        call Highlight("CursorColumn", s:white, s:none, s:bold)
        call Highlight("ColorColumn", s:white, s:light_charcoal, s:bold)

        call Highlight("TabLine", s:none, s:dark_grey, s:none)
        call Highlight("TabLineSel", s:none, s:charcoal, s:bold)
        call Highlight("TabLineFill", s:none, s:darker_grey, s:none)

        call Highlight("StatusLine", s:default_grey, s:dark_grey, s:none)
        call Highlight("StatusLineNC", s:light_grey, s:darker_grey, s:none)

        call Highlight("SignColumn", s:none, s:none, s:none)
        call Highlight("NonText", s:darker_grey, s:none, s:none)
        call Highlight("LineNr", s:darker_grey, s:none, s:none)
        call Highlight("VertSplit", s:darker_grey, s:charcoal, s:none)

        call Highlight("Visual", s:green, s:light_charcoal, s:none)

        call Highlight("Error", s:white, s:blood_red, s:none)
        call Highlight("ErrorMsg", s:white, s:blood_red, s:none)
        call Highlight("WarningMsg", s:none, s:orange, s:none)

        call Highlight("SpecialKey", s:dark_grey, s:darker_grey, s:none)
        call Highlight("IncSearch", s:light_grey, s:dark_red, s:none)
        call Highlight("Search", s:light_grey, s:dark_red, s:bold_underline)


    " Git -------------------------------------------
        call Highlight("diffChange", s:orange, s:none, s:none)
        call Highlight("diffText", s:default_grey, s:none, s:none)
        call Highlight("diffDelete", s:danger, s:none, s:none)

        call Highlight("diffAdd", s:green, s:none, s:none)
        call Highlight("diffAdded", s:light_green, s:none, s:none)
        call Highlight("diffRemoved", s:magenta, s:none, s:none)

        call Highlight("diffFile", s:default_grey, s:none, s:none)
        call Highlight("diffLine", s:purple, s:none, s:none)
        call Highlight("diffIndexLine", s:purple, s:none, s:none)

        call Highlight("gitcommitHeader", s:light_blue, s:none, s:none)
        call Highlight("gitcommitSelectedFile", s:orange, s:none, s:none)
        call Highlight("gitcommitSummary", s:default_grey, s:none, s:none)
        call Highlight("gitcommitOverflow", s:magenta, s:none, s:none)

    " vim syntax -------------------------------------------
        call Highlight("vimParenSep", s:default_grey, s:none, s:bold)
        call Highlight("vimOperParen", s:light_blue, s:none, s:none)
        call Highlight("vimUserFunc", s:purple, s:none, s:none)
        call Highlight("vimFunction", s:orange, s:none, s:none)

    " XML highlighting -------------------------------------------
        hi def link xmlTodo   Todo
        call Highlight("xmlTag", s:light_blue, s:none, s:none)
        call Highlight("xmlTagName", s:light_blue, s:none, s:none)
        call Highlight("xmlEndTag", s:light_blue, s:none, s:none)
        call Highlight("xmlEqual", s:magenta, s:none, s:none)

    " JSON highlighting -------------------------------------------
        call Highlight("jsonKeyword", s:light_blue, s:none, s:none)
        call Highlight("jsonString", s:dark_brown, s:none, s:none)

    " NERDTree highlighting -------------------------------------------
        call Highlight("NERDTreeClosable", s:dark_brown, s:none, s:none)
        call Highlight("NERDTreeOpenable", s:dark_brown, s:none, s:none)
        call Highlight("NERDTreeDirSlash", s:light_blue, s:none, s:none)
        call Highlight("NERDTreeFile", s:none, s:none, s:none)


    " Tagbar support
        " The help at the top of the buffer.
            call Highlight("TagbarComment", s:danger, s:none, s:none)
        " The header of generic "kinds" like "functions" and "variables".
            call Highlight("TagbarKind", s:dark_brown, s:none, s:bold_underline)
        " The "kind" headers in square brackets inside of scopes.
            call Highlight("TagbarNestedKind", s:dark_brown, s:none, s:none)

        " Tags that define a scope like classes, structs etc.
            call Highlight("TagbarScope", s:dark_brown, s:none, s:none)
        " The type of a tag or scope if available.
            call Highlight("TagbarType", s:none, s:none, s:none)
        " Function signatures.
            call Highlight("TagbarSignature", s:none, s:none, s:none)

        " The asterisk (*) that signifies a pseudo-tag.
            call Highlight("TagbarPseudoID", s:none, s:none, s:none)
        " The fold icon on the left of foldable tags.
            call Highlight("TagbarFoldIcon", s:danger, s:none, s:bold)
        " The colour that is used for automatically highlighting the current tag.
            call Highlight("TagbarHighlight", s:danger, s:black, s:none)

        " The "public" visibility symbol.
            call Highlight("TagbarVisibilityPublic", s:dark_brown, s:none, s:bold)
        " The "protected" visibility symbol.
            call Highlight("TagbarVisibilityProtected", s:dark_brown, s:none, s:bold)
        " The "private" visibility symbol.
            call Highlight("TagbarVisibilityPrivate", s:dark_brown, s:none, s:bold)


    " Signify support
        call Highlight("SignifyLineAdd", s:green, s:none, s:none)
        call Highlight("SignifyLineChange", s:black, s:none, s:bold)
        call Highlight("SignifyLineDelete", s:danger, s:none, s:none)

    " Ale linter/fixer support
        call Highlight("ALESignColumnWithErrors", s:red, s:none, s:bold)
        call Highlight("ALESignColumnWithoutErrors", s:red, s:none, s:bold)
        call Highlight("ALEError", s:white, s:red, s:none)
  

        call Highlight("ALEErrorLine", s:none, s:none, s:none)
        call Highlight("ALEErrorSign", s:white, s:red, s:none)
        call Highlight("ALEErrorSignLineNr", s:white, s:red, s:none)

        call Highlight("ALEInfo", s:none, s:none, s:bold)
        call Highlight("ALEInfoSign", s:orange, s:none, s:none)
        call Highlight("ALEInfoLine", s:none, s:none, s:none)

        call Highlight("ALEInfoSignLineNr", s:orange, s:none, s:none)
        call Highlight("ALEStyleError", s:red, s:none, s:none)
        call Highlight("ALEStyleErrorSign", s:red, s:none, s:none)

        call Highlight("ALEStyleErrorSignLineNr", s:white, s:red, s:none)
        call Highlight("ALEStyleWarning", s:orange, s:none, s:none)
        call Highlight("ALEStyleWarningSign", s:white, s:orange, s:none)

        call Highlight("ALEStyleWarningSignLineNr", s:orange, s:none, s:none)
        call Highlight("ALEVirtualTextError", s:white, s:red, s:none)
        call Highlight("ALEVirtualTextInfo", s:white, s:none, s:bold)

        call Highlight("ALEVirtualTextStyleError", s:white, s:red, s:none)
        call Highlight("ALEVirtualTextStyleWarning", s:orange, s:none, s:none)
        call Highlight("ALEVirtualTextWarning", s:orange, s:none, s:none)

        call Highlight("ALEWarning", s:orange, s:none, s:none)
        call Highlight("ALEWarningLine", s:orange, s:none, s:underline)
        call Highlight("ALEWarningSign", s:white, s:orange, s:none)

        call Highlight("SignifyLineDeleteFirstLine", s:danger, s:none, s:none)
        call Highlight("ALEWarningSignLineNr", s:white, s:orange, s:none)


" Haskell-vim support    (Haskell syntax plugin)
" ------------------
        call Highlight("HaskellOperators", s:light_blue, s:none, s:none)
        call Highlight("HaskellDelimiter", s:sea_blue, s:none, s:none)
        call Highlight("haskellSeparator", s:none, s:none, s:none)
        call Highlight("haskellNumber", s:danger, s:none, s:none)
        call Highlight("haskellIdentifier", s:danger, s:none, s:none)
        call Highlight("haskellWhere", s:danger, s:none, s:underline)
        call Highlight("haskellKeyword", s:danger, s:none, s:underline)
        call Highlight("haskellLineComment", s:grey, s:none, s:none)
        call Highlight("haskellBlockComment", s:grey, s:none, s:none)

        call Highlight("haskellPragma", s:green, s:none, s:none)
        call Highlight("haskellLiquid", s:green, s:none, s:none)
        call Highlight("haskellQuasiQuoted", s:green, s:none, s:none)
        call Highlight("haskellDefaultTypes", s:green, s:none, s:none)


" Fuzzy Finder support
" ------------------

"  See https://github.com/junegunn/fzf.vim
    let g:fzf_colors =
    \ { 'fg':      ['fg', 'Normal'],
      \ 'bg':      ['bg', 'Normal'],
      \ 'hl':      ['fg', 'Comment'],
      \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
      \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
      \ 'hl+':     ['fg', 'Statement'],
      \ 'info':    ['fg', 'PreProc'],
      \ 'border':  ['fg', 'Ignore'],
      \ 'prompt':  ['fg', 'Conditional'],
      \ 'pointer': ['fg', 'Exception'],
      \ 'marker':  ['fg', 'Keyword'],
      \ 'spinner': ['fg', 'Label'],
      \ 'header':  ['fg', 'Comment'] }
