# WARM
This is a vim/nvim colorscheme that supports tty colors, 255 term colors
and gui colors.

!(screenshot)[img/preview.png]

---

## INSTALLING
To install this colorscheme we recommend Plug, a vim plugin manager.
See more about in [github/junegunn/vim-plug](https://github.com/junegunn/vim-plug).

### Plug
~~~ vim
" Put this line on your .vimrc
    Plug 'https://gitlab.com/agustinjimenez/warm.git'

~~~

~~~ vim
" Save and load the changes in your current sesion
    :w!
    :so %

" Install this plugin
    :PlugInstall

~~~

### Manual Install
If you choose the manual installation see the following steps:
~~~ sh
# Clone this repo
    git clone https://gitlab.com/agustinjimenez/warm.git

# Change your current directory
    cd warm

# Move the colorscheme file to your own colorschemes folder

    # If you're using vim:
        mv colors/warm.vim ~/.vim/colors/

    # If you're using nvim:
        mv colors/warm.vim {YOU-NEOVIM-CONFIG-DIRECTORY}/colors/
~~~

---

# Contributing

### Contributors are wellcome!

**Any help will be appreciated**. 

- Creating an issue.
- Merge Requests
- Fixing bugs.
- Improve the documentation.

Please, send me an email if you have other needs about this project:
agustin_jimenez@tutanota.com

**Thank You!**

---

# License
Licensed under the MIT License.
See more in the [LICENSE](LICENSE)

# Authors
Development lead and mainteined by [Agustin Jimenez](http://agustinjimenez.gitlab.io/portfolio)
